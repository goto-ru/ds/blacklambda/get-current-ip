{ pkgs, mkDerivation, base, bytestring, lens, stdenv, wreq }:
mkDerivation {
  pname = "get-current-ip";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [ base bytestring lens wreq ];
  buildDepends = with pkgs; [ cabal-install ];
  license = stdenv.lib.licenses.agpl3;
}
