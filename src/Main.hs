module Main where

import Network.Wreq
import Control.Lens
import qualified Data.ByteString.Lazy as DBSL

main :: IO ()
main = do
    r <- get "https://icanhazip.com"
    DBSL.putStr $ r ^. responseBody
